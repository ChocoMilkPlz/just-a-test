# Just a small front end test

## Quickly Viewing the last build
I included a built version on /dist, you can simply drag the contents into any hosted location and view it straight away. You can also just drag the html file onto Chrome as well, and use devtools to play around with device emulators etc..

## Development Framework
Vue

## Development Requirements 
Webpack version: 4
Currently tested on: Windows 10

## Developing and building
You will need node and npm and node in your system in order to develop and build

First of all, in order to install dependencies that are not committed to this template but are needed for development and building, run:

```javascript
// In project root
npm install
```

To run in development mode:
(Note: The dev server can be accessed through local network on port 8080 for testing with devices)

```javascript
// Will open a browser window using webpack dev server on localhost:8080
npm start

// Same but will not open a new browser window
npm run dev
```


To build in order to deploy somewhere:

```javascript
// Output will be in the /dist folder
npm run build
```












