const path = require('path')
const config = require('./settings')
const HtmlWebPackPlugin = require("html-webpack-plugin")
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin")


// Plugins we will pass to webpack
const plugins = []

// Proxy settings for dev server to test APIs
const proxySettings = {}
proxySettings[config.proxyOptions.path] = {
    target: config.proxyOptions.url,
    secure: false,
    changeOrigin: true
}

// CSS Modules
let cssConfig = {}
if(config.useCSSModules) {
    cssConfig = {
        modules: true,
        sourceMap: true,
        importLoaders: 1,
        localIdentName: '[name]_[local]_[hash:base64]',
        minimize: true
    }
}

// Development Aliases to resolve imports
const aliases = config.aliases
for(let a in aliases) {
    aliases[a] = path.resolve(__dirname, aliases[a])
}

// ** Copying ** //
// Thing we are copying to ./dist
let copyObject = config.filesToCopy.browser || []

// Create plugin instance with appropriate arguments
let copyPlugin = new CopyWebpackPlugin(copyObject)

// *********** //


// ** Extract CSS into an actual file ** //
extractCSSPlugin = new MiniCssExtractPlugin({
    // Options similar to the same options in webpackOptions.output
    // both options are optional
    filename: (config.flattenOutput) ? ((config.usePages) ? `[chunkhash].css` : `./[chunkhash].css` )  : `${config.outputPaths.styles}/[chunkhash].css`,
    chunkFilename: '[id].[chunkhash].css',
    publicPath: ''
})
// ************************************ //


// ** Template ** //
let htmlWebPackPlugins = []
let entryObject


entryObject = './src/index.js'
htmlWebPackPlugins = [new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "index.html"
})]
// ************ //




// Append Plugins 
plugins.push(copyPlugin)
plugins.push(extractCSSPlugin)
plugins.push(new VueLoaderPlugin())

for(let h in htmlWebPackPlugins) {
    plugins.push(htmlWebPackPlugins[h])
}


// Note: It is wrapped in a function so that we can obtain argv.mode
module.exports = (env, argv) => ({
    entry: entryObject,
    output: {
        path: path.resolve(__dirname, "../dist"),
        filename: (config.flattenOutput) ? `./bundle.[chunkhash].js` : `${config.outputPaths.scripts}/bundle.[chunkhash].js`,
        publicPath: ''
    },
    module: {
        rules: [
        {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
            }
        },
        {
            test: /\.scss|sass|css$/,
            use: [
                {
                    loader: (config.extractCSSToFile) ? MiniCssExtractPlugin.loader : 'style-loader', options: (config.extractCSSToFile && !config.flattenOutput) ? { publicPath:'../.' } : {}
                },
                {   
                    loader: 'css-loader',
                    query: cssConfig
                },
                'resolve-url-loader',
                'sass-loader?sourceMap'
            ]
        },
        { 
            test: /\.(jpe?g|gif|png|svg|ico|woff|ttf|eot|otf|woff|woff2|wav|avi|ogg|mp3|mp4)$/,
            use: [
                {
                    loader: "file-loader",
                    options: {
                        publicPath: '',
                        name: (config.flattenOutput) ? `./[hash].[ext]` : `${config.outputPaths.default}/[hash].[ext]`
                    }
                }
            ]
        },
        {
            test: /\.vue$/,
            loader: 'vue-loader'
        }
      ]
    },

    optimization: {
        minimize: !config.skipMinify
    },
    
    devServer: {
        stats: "minimal",
        proxy: proxySettings,
        port: config.devServerPort.browser,
        public: 'localhost:' + config.devServerPort.browser
    },

    // https://webpack.js.org/configuration/devtool/
    devtool: (argv.mode == 'production') ? 'nosources-source-map' : 'eval-source-map',

    resolve: {
        extensions: ['*', '.js', '.vue', '.json'],
        alias: aliases
    },

    // No difference right now
    plugins: (argv.mode == 'production') ? plugins : plugins

    }
)
