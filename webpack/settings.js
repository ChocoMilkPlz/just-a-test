const config = {

    // Dev Server Ports for Browser / Electron
    devServerPort: {
        browser: 8080
    },

    // What files/folders do we directly copy when building for Browser / Electron
    filesToCopy: {
        browser: [
            {from: './static', to: './static'}
        ],
    },


    // output paths for keeping everything organized on dist (unless flatten output is set to true...)
    flattenOutput: false,
    outputPaths: {
        styles: './assets/styles',
        scripts: './assets/scripts',
        default: './assets'
    },

    // CSS options:
    useCSSModules: false, // CSS Modules
    extractCSSToFile: true, // Extract CSS and inject into head? (Lets CSS load as early as possible)

    // If dev server receives a match for url, redirect request to:
    // Useful for testing APIs
    proxyOptions: {
        path: '/api/**',
        url: 'http://localhost:3000'
    },

    // Aliases for importing files within Webpack.
    // Append any other alias needed specific to projects.
    aliases: {
        Components: '../src/components',
        Store: '../src/store',
        Routes: '../src/routes',
        Libraries: '../src/lib',
        Assets: '../src/assets',
        Tools: '../src/tools',
        Styles: '../src/styles',
        Api: '../src/api',
    },

    // For potential debugging or special cases?
    skipMinify: false
}



module.exports =  config