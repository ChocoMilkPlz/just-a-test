import context from './modules/context'
import scheduling from './modules/scheduling'
import sites from './modules/sites'

const store = {
    modules: {
        context,
        scheduling,
        sites,
    }
}

export default store