/*** Context Module ***/
// Takes care of all state related to the context the page is running on.
// Did it break a mobile threshold? Is it running within electron? Unified place to obtain viewport info as well when needed.
import context from 'Tools/context'

const mobileThreshold = 900 
const initialClientSize = context.clientSize()

// Initial State
const state = {
    viewport: {
        width: initialClientSize.width,
        height: initialClientSize.height
    },

    mobile: context.isMobile(mobileThreshold),
    electron: context.isElectron(),
}


// getters
const getters = {
}

// actions
const actions = {
    refreshScreenDimensions ({ commit, state }) {
        let isMobile = context.isMobile(mobileThreshold)
        let viewport = context.clientSize()
        commit('SET_SCREEN_DIMENSIONS', { width: viewport.width, height: viewport.height })
        if(state.mobile != isMobile) {
            commit('SET_MOBILE', { isMobile: isMobile })
        }
    },

    scrolledToBottom ({commit, state, rootState, dispatch }) {
        if(rootState.route.fullPath == '/') {
            dispatch('scheduling/expandSiteBucket', {}, {root: true})
        }
    }
}


// mutations
const mutations = {
    SET_SCREEN_DIMENSIONS (state, payload) {
        state.viewport.width = payload.width
        state.viewport.height = payload.height
    },

    SET_MOBILE (state, payload) {
        state.mobile = payload.isMobile
    }
}

// Export
export default {
    namespaced : true,
    state,
    getters,
    actions,
    mutations
}