/*** Scheduling Module ***/
// Takes care of all the state relating to the main 'Scheduling' components on the website.

import { filterByTag, filterBySort, filterByString } from 'Tools/filtering/scheduling';
import _find from 'lodash.find'
import { getSite } from 'Api/sites'

// Initial State
const state = {
    // This is the list of sites that we actually see on the Site List component.
    // It is populated internally by filtering the data from the sites state module.
    siteBucket: [],

    // Filters applied to sites source data.
    // state.siteBucket will always be filtered by these.
    filters: {
        tag: '', // Tags: '', 'new', 'old', 'renovated', 'state', 'company', 'individual'
        sort: '', // Sort types: '', 'alpha_asc', 'alpha_desc', 'created_asc', 'created_desc', 'updated_asc', 'updated_desc'
        search: '', // Full String Search.
    },

    // The Site Details component sometimes wants to load a single site fairly quickly.
    // We do not want to depend on the siteBucket nor the Sites module having downloaded this data.
    // Specially if it is the initial page that we load (Ex: Bookmark /sites/mNe7n-jTZlC)
    specificSite: null,
    failedSpecificFetch: false,
}


// getters
const getters = {
    
    // Site that will be displayed on the Site Details Component
    // Note that it tries to obtain it from the master list of sites from the sites vuex module if available
    // If not it will use the single fetched site if it exists.
    getSiteToDisplay (state, getters, rootState, rootGetters) {
        if(rootState.route.params.id) {
            if (rootState.sites.downloaded) {
                return rootGetters['sites/getSiteByID'](rootState.route.params.id)
            } else if(state.specificSite && state.specificSite.id == rootState.route.params.id) {
                return state.specificSite
            } 
        } else {
            return ''
        }
    },

    // This filtering getter is what the siteBucket uses internally as filtered source of info.
    filteredSites (state, getters, rootState, rootGetters) {
        let filteredSites = rootState.sites.sites
        if(state.filters.tag) {
            filteredSites = filterByTag(filteredSites, state.filters.tag)
        }

        if(state.filters.search && typeof state.filters.search === 'string') { 
            filteredSites = filterByString(filteredSites, state.filters.search)
        }

        if(state.filters.sort) {
            filteredSites = filterBySort(filteredSites, state.filters.sort)
        }
        
        return filteredSites
    }
}

// actions
const actions = {

    // Select a site from list of sites.
    selectSite ({ commit, state, rootState }, payload) {
        if(!payload || !payload.id) return;
        vueRouter.push(`/sites/${payload.id}`)
    },

    // Go back to list.
    deselectSite ({ commit, state }, payload) {
        vueRouter.push(`/`)
    },


    // Set the filters, and reload the siteBucket with said filters.
    setSiteBucketFilters ({ commit, rootState, dispatch }, payload) {
        dispatch('emptySiteBucket')
        commit('SET_SITE_BUCKET_FILTERS',  payload)
        dispatch('expandSiteBucket')
    },

    // Empty site bucket used for rendering Site List
    emptySiteBucket ({ commit }, payload) {
        commit('EMPTY_SITE_BUCKET')
    },

    // Expand site bucket by 10, or as much as possible.
    // filtered sites source has been cleaned up to ensure no duplicate IDs
    expandSiteBucket ({ commit, state, getters, rootState }, payload) {
        let currentLength = state.siteBucket.length
        let sitesToAdd = getters.filteredSites.slice(currentLength, currentLength + 10) || []

        commit('EXPAND_SITE_BUCKET', { sites: sitesToAdd })
    },


    // Wrapper around Api/getSite
    // Fetches a single site for Site Details Component to use. In case that component wants to load that site independently of sites' module site list.
    fetchSingleSite ({commit, state, dispatch}, payload) {
        if(!payload || !payload.id) return;

        getSite(payload.id).then(site => {
            commit('FETCHED_SPECIFIC_SITE', { site: site })
        }).catch(error => {
            commit('FAILED_SPECIFIC_FETCH')
            console.error(error)
        })
    },
}


// mutations
const mutations = {

    EMPTY_SITE_BUCKET (state, payload) {
        state.siteBucket = []
    },

    EXPAND_SITE_BUCKET (state, payload) {
        state.siteBucket = state.siteBucket.concat(payload.sites)
    },

    SET_SITE_BUCKET_FILTERS (state, payload) {
        for(let p in payload) {
            state.filters[p] = payload[p]
        }
    },

    FETCHED_SPECIFIC_SITE (state, payload) {
        state.specificSite = payload.site
    },

    FAILED_SPECIFIC_FETCH (state, payload) {
        state.failedSpecificFetch = true
    }
}

// Export
export default {
    namespaced : true,
    state,
    getters,
    actions,
    mutations
}