/*** Sites Module ***/
// Abstracts all site functionality and contains master source of truth for sites used on this instance of web app.

import _find from 'lodash.find'
import { getSites } from 'Api/sites'

// Initial State
const state = {
    // Network state
    downloading: false,
    downloaded : false,
    failedDownload: false,

    // Master List of sites
    sites: [],
}


// getters
const getters = {
    // Find a site by its' ID. Mostly for externaly use by other modules.
    getSiteByID: (state) => (id) => {
        return _find(state.sites, { id: id } )
    }
}

// actions
const actions = {
    // Fetch All Sites available.
    // If Successful, dispatch an action letting the scheduling module to expand its bucket of sites it will list on the Site List component
    fetchSites ({commit, state, dispatch}, payload) {
        if(state.downloading) return;
        commit('BEGIN_FETCHING_SITES')

        getSites().then(sites => {
            commit('FETCHED_SITES', { sites: sites })
            dispatch('scheduling/expandSiteBucket', {}, { root: true })
        }).catch(error => {
            commit('FAILED_FETCHED_SITES')
            console.error(error)
        })
    }
}


// mutations
const mutations = {
    BEGIN_FETCHING_SITES (state, payload) {
        state.downloading = true
        state.failedDownload = false
    },

    FETCHED_SITES (state, payload) {
        state.sites = payload.sites
        state.failedDownload = false
        state.downloading = false
        state.downloaded = true
    },

    FAILED_FETCHED_SITES (state, payload) {
        state.failedDownload = true
        state.downloading = false
        state.downloaded = false
    },
}

// Export
export default {
    namespaced : true,
    state,
    getters,
    actions,
    mutations
}