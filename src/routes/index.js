import SiteList from 'Components/views/SiteList'
import SiteDetails from 'Components/views/SiteDetails'

const routes = [
    { path: '/', component: SiteList },
    { path: '/sites/:id', component: SiteDetails }
]

export default routes