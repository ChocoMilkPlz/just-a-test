import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faEnvelope, faMapMarkerAlt, faPhone, faRedo, faSortAmountDown, faSearch, faCloudDownloadAlt, faBars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUser)
library.add(faEnvelope)
library.add(faMapMarkerAlt)
library.add(faPhone)
library.add(faRedo)
library.add(faSortAmountDown)
library.add(faSearch)
library.add(faCloudDownloadAlt)
library.add(faBars)

export default FontAwesomeIcon