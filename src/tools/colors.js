const getRandomColor = () => {
    let letters = '3456789ABC';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 10)];
    }
    return color;
}


export {
    getRandomColor
}