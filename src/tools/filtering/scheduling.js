import _filter from 'lodash.filter'
import _orderBy from 'lodash.orderby'


const filterByTag = (siteArray, tag) => {
    return _filter(siteArray, (o) => { for(let t in o.tags) { if(o.tags[t] == tag) { return true; } }})
}


const filterByString = (siteArray, string) => {
    let lowerSearch = string.toLowerCase()
    return _filter(siteArray, (o) => {
        if(o.title && o.title.toLowerCase().indexOf(lowerSearch) !== -1) {
            return true;
        }

        if(o.contacts && o.contacts.main) {
            if(o.contacts.main.firstName && o.contacts.main.firstName.toLowerCase().indexOf(lowerSearch) !== -1) {
                return true;
            }
            if(o.contacts.main.lastName && o.contacts.main.lastName.toLowerCase().indexOf(lowerSearch) !== -1) {
                return true;
            }
            if(o.contacts.main.email && o.contacts.main.email.toLowerCase().indexOf(lowerSearch) !== -1) {
                return true;
            }
            if(o.contacts.main.jobTitle && o.contacts.main.jobTitle.toLowerCase().indexOf(lowerSearch) !== -1) {
                return true;
            }
            if(o.contacts.main.address) {
                if(o.contacts.main.address.city && o.contacts.main.address.city.toLowerCase().indexOf(lowerSearch) !== -1) {
                    return true;
                }

                if(o.contacts.main.address.street && o.contacts.main.address.street.toLowerCase().indexOf(lowerSearch) !== -1) {
                    return true;
                }

                if(o.contacts.main.address.country && o.contacts.main.address.country.toLowerCase().indexOf(lowerSearch) !== -1) {
                    return true;
                }

                if(o.contacts.main.address.state && o.contacts.main.address.state.toLowerCase().indexOf(lowerSearch) !== -1) {
                    return true;
                }
            }
        }

        if(o.address) {
            if(o.address.city && o.address.city.toLowerCase().indexOf(lowerSearch) !== -1) {
                return true;
            }

            if(o.address.street && o.address.street.toLowerCase().indexOf(lowerSearch) !== -1) {
                return true;
            }

            if(o.address.country && o.address.country.toLowerCase().indexOf(lowerSearch) !== -1) {
                return true;
            }

            if(o.address.state && o.address.state.toLowerCase().indexOf(lowerSearch) !== -1) {
                return true;
            }
        }
    })
}


const filterBySort = (siteArray, sortType) => {
    let filteredSites = siteArray
    if(sortType == 'alpha_desc') {
        filteredSites = _orderBy(filteredSites, ['title'], ['asc'])
    } else if(sortType == 'alpha_asc') {
        filteredSites = _orderBy(filteredSites, ['title'], ['desc'])
    } else if(sortType == 'created_desc') {
        filteredSites = _orderBy(filteredSites, (o) => {
            if(o.createdAt && typeof o.createdAt === 'string') {
                return new Date(o.createdAt).getTime()
            } else {
                return 0;
            }
        }, ['desc'])
    } else if(sortType == 'created_asc') {
        filteredSites = _orderBy(filteredSites, (o) => {
            if(o.createdAt && typeof o.createdAt === 'string') {
                return new Date(o.createdAt).getTime()
            } else {
                return 0;
            }
        }, ['asc'])
    } else if(sortType == 'updated_desc') {
        filteredSites = _orderBy(filteredSites, (o) => {
            if(o.updatedAt && typeof o.updatedAt === 'string') {
                return new Date(o.updatedAt).getTime()
            } else {
                return 0;
            }
        }, ['desc'])
    } else if(sortType == 'updated_asc') {
        filteredSites = _orderBy(filteredSites, (o) => {
            if(o.updatedAt && typeof o.updatedAt === 'string') {
                return new Date(o.updatedAt).getTime()
            } else {
                return 0;
            }
        }, ['asc'])
    }
    return filteredSites
}


export {
    filterByTag,
    filterBySort,
    filterByString
}