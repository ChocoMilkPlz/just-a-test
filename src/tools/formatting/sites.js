

const formatTitle = (siteToFormat) => {
    if(!siteToFormat) return '';
    return siteToFormat.title || ''
}

const formatAddress = (siteToFormat) => {
    if(!siteToFormat) return '';
    let address = siteToFormat.address
    if(address) {
        return `${address.street}, ${address.state}` || ''
    } else {
        return ''
    }
}

const formatContact = (siteToFormat) => {
    if(!siteToFormat) return '';
    let contacts = siteToFormat.contacts
    if(contacts && contacts.main) {
        return `${contacts.main.lastName} ${contacts.main.firstName}` || ''
    } else {
        return ''
    }
}

const formatDetailsContact = (siteToFormat) => {
    if(siteToFormat && siteToFormat.contacts && siteToFormat.contacts.main) {
        return `${siteToFormat.contacts.main.firstName || ''} ${siteToFormat.contacts.main.lastName || ''}` 
    }
    return ''
}

const formatDetailsTitle = (siteToFormat) => {
    if(siteToFormat && siteToFormat.contacts && siteToFormat.contacts.main) {
        return `${siteToFormat.contacts.main.jobTitle || ''}` 
    }

    return ''
}

const formatDetailsPhone = (siteToFormat) => {
    if(siteToFormat && siteToFormat.contacts && siteToFormat.contacts.main) {
        return `${siteToFormat.contacts.main.phoneNumber || ''}` 
    }

    return ''
}

const formatDetailsEmail = (siteToFormat) => {
    if(siteToFormat && siteToFormat.contacts && siteToFormat.contacts.main) {
        return `${siteToFormat.contacts.main.email || ''}` 
    }

    return ''
}

const formatDetailsAddressStreet = (siteToFormat) => {
    if(siteToFormat && siteToFormat.address) {
        return `${siteToFormat.address.street || ''}`
    }

    return ''
}

const formatDetailsAddressCity = (siteToFormat) => {
    if(siteToFormat && siteToFormat.address) {
        return `${siteToFormat.address.city || ''}`
    }

    return ''
}

const formatDetailsAddressZipCode = (siteToFormat) => {
    if(siteToFormat && siteToFormat.address) {
        return `${siteToFormat.address.zipCode || ''}`
    }

    return ''
}

const formatDetailsAddressState = (siteToFormat) => {
    if(siteToFormat && siteToFormat.address) {
        return `${siteToFormat.address.state || ''}`
    }

    return ''
}

const formatDetailsAddressCountry = (siteToFormat) => {
    if(siteToFormat && siteToFormat.address) {
        return `${siteToFormat.address.country || ''}`
    }

    return ''
}

export {
    formatTitle,
    formatAddress,
    formatContact,
    formatDetailsContact,
    formatDetailsTitle,
    formatDetailsPhone,
    formatDetailsEmail,
    formatDetailsAddressStreet,
    formatDetailsAddressCity,
    formatDetailsAddressZipCode,
    formatDetailsAddressState,
    formatDetailsAddressCountry
}



