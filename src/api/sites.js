import axios from 'axios'
const baseAPIURL = 'https://tracktik-frontend-challenge-luvyqpolgv.now.sh'
import { getRandomColor } from 'Tools/colors'

const getSites = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${baseAPIURL}/sites`).then(response => {
            if(response && response.data) {
                if(!response.data.slice) {
                    return reject({ responseInvalid: true});
                }
                return resolve( enhanceSites( removeDuplicates(response.data) ))
            } else {
                return reject({ responseInvalid: true })
            }
        }).catch(err => {
            return reject({ error: err, requestFailed: true })
        }) 

    })
}

const getSite = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${baseAPIURL}/sites?id=${id}`).then(response => {
            if(response && response.data && response.data[0]) {
                return resolve( enhanceSites(response.data)[0] )
            } else {
                return reject({ responseInvalid: true })
            }
        }).catch(err => {
            return reject({ error: err, requestFailed: true })
        }) 

    })
}

// Just in case
const removeDuplicates = (sites) => {
    let IDs = {}
    let i = sites.length
    while(i--) {
        if(IDs[sites[i].id]) {
            sites.splice(i, 1)
        } else {
            IDs[sites[i].id] = true
        }
    }
    return sites
}

// Add a default color 
const enhanceSites = (sites) => {
    for(let s in sites) {
        sites[s]._color = getRandomColor()
    }
    return sites
}

export {
    getSites,
    getSite
}
