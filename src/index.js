// Import Global Style for this page
import mainStyle from './index.scss'

// Import Polyfills and others from Tools
import 'Tools/polyfills'
import context from 'Tools/context'

// Vue Dependencies
import Vue from 'vue'
import VueX from 'vuex'
import VueRouter from 'vue-router'
import { sync } from 'vuex-router-sync'

// Font Awesome Integration with Vue
import iconComponent from 'Components/font-awesome-icon/FontAwesomeIcon'
Vue.component('font-awesome-icon', iconComponent)

// Tell Vue what modules we are using.
Vue.use(VueX)
Vue.use(VueRouter)

// Create Router and Store from definitions
import storeDefintion from 'Store'
import routesDefinition from 'Routes'

const store = new VueX.Store(storeDefintion)
const router = new VueRouter({
    routes: routesDefinition
})

// Make Available so that it can be used globally.
// Ex: Through VueX actions.
window.vueRouter = router

sync(store, router)

// Main Vue Instance and Component
import App from 'Components/App'

let vm = new Vue({
    el: '#root',
    store,
    router,
    components: {App} ,
    render: f => f(App)
})

// For Console Testing.
window.vm = vm

// Handle browser resize
import debounce from 'lodash.debounce'

const onResize = () => { store.dispatch('context/refreshScreenDimensions') }
const onResizeDebounced = debounce(onResize, 150)

window.addEventListener('resize', () => {
    onResizeDebounced()
})


// Handle Browser scroll to bottom.
const onScroll = () => { 
    let limit = Math.max( document.body.scrollHeight, document.body.offsetHeight, 
        document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight ) - window.innerHeight;
    
    let scrollPosition =  context.getScrollTop()

    if(scrollPosition >= limit - 120) {
        store.dispatch('context/scrolledToBottom')
    }
}
// const onScrollDebounced = debounce(onScroll, 10)
window.addEventListener('scroll', (e) => {
    onScroll()
})

